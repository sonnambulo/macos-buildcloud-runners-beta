<!---
For the issue title enter: macOS Build Cloud Beta access request for <organization name>
--->

## Runner SaaS macOS Beta access request form

If you are interested in participating in the Runner SaaS macOS beta, then fill out the details below.

--------------------------------------------------------------------------------

### Contact information

- Name:
- Company:
- Role:

### Do you primarily use GitLab's SaaS (GitLab.com) or self-managed option?

- [ ] SaaS (GitLab.com)
- [ ] Self-managed


### Approximately how many projects will you need to build on the Runner SaaS: macOS Runners when it is GA?

<!-- Enter the number of projects that you estimate will need to use the macOS Build Cloud -->

- No of projects:


### Approximately how many CI builds per week does your team run?

<!-- Enter the number of builds that you expect to execute per week -->


### Which version of Xcode do you need to build your software?

<!-- Enter the version of Xcode you use. We support the last 3 major versions -->

- Xcode versions:

### Project or Group URL

<!-- Enter the group or project URL that you would like to use for the beta -->

- Enter the group or project URL that you would like to use for the beta:

/assign @DarrenEastman
/confidential
