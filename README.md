## Runner SaaS: macOS Runners on GitLab.com  - (BETA)

- Thanks for your interest in the Runner SaaS: macOS beta. To help ensure the best possible experience for customers and the open-source projects on GitLab.com, access to the beta is limited. 

-  We will begin onboarding new customers to the beta starting on July 22, 2021 (GitLab 14.1). 

- To sign up, open an [issue](https://gitlab.com/gitlab-com/macos-buildcloud-runners-beta/-/issues/new?issue%5Bmilestone_id%5D=) in this project and select the beta_access_request template.


- We will use that issue to communicate your onboarding status and provide other details for the use of the  Beta.  

- Pricing details for the Runner SaaS: macOS runner offerings will be made available in conjunction with general availability - currently targeted for GitLab 15.0

- To follow along on the GA status, refer to this [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/342848).


- If you have any questions about our plans for Open Beta or would like to discuss specific requirements for your team, please mention @DarrenEastman on the Open Beta issue.

## Scheduled Maintenance


- None scheduled at this time.

